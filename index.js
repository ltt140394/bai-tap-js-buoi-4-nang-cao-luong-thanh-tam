function getMyEle(id) {
    return document.getElementById(id);
}

// Bài tập 1
function tinhNgayHomQua() {
    var ngayValue = getMyEle("number_ngay_bt1").value * 1;
    var thangValue = getMyEle("number_thang_bt1").value * 1;
    var namValue = getMyEle("number_nam_bt1").value * 1;

    if (ngayValue <= 0 || ngayValue > 31) {
        getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày không chính xác. Vui lòng nhập lại`
    } else if (thangValue <= 0 || thangValue > 12) {
        getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Tháng không chính xác. Vui lòng nhập lại`
    } else if (namValue <= 0) {
        getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Năm không chính xác. Vui lòng nhập lại`
    } else {
        if (thangValue == 1) { /* Do tháng 1 là tháng đầu năm, nếu rơi vào ngày đầu tháng 1 thì năm của ngày hôm qua sẽ là năm ngoái nên xử lý riêng */
            if (ngayValue == 1) { /* Ngày đầu tháng 1 */
                ngayValue = 31;
                thangValue = 12;
                namValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                ngayValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            }
        } else if (thangValue == 2) { /* Tháng 2 có 28 ngày */
            if (ngayValue == 1) { /* Ngày đầu tháng 2 */
                ngayValue = 31;
                thangValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else if (ngayValue <= 28) {
                ngayValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Tháng 2  chỉ có tối đa 28 ngày. Vui lòng nhập lại `
            }
        } else if (thangValue == 4 || thangValue == 6 || thangValue == 9 || thangValue == 11) { /*Các tháng có 30 ngày */
            if (ngayValue == 1) { /* Ngày đầu các tháng có 30 ngày */
                ngayValue = 31;
                thangValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else if (ngayValue <= 30) {
                ngayValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Tháng ${thangValue} chỉ có tối đa 30 ngày. Vui lòng nhập lại`
            }
        } else if (thangValue == 8) { /* Do trước tháng 8 là tháng 7, 2 tháng này có 31 ngày liền kề nhau nên phải xử lý riêng */
            if (ngayValue == 1) { /* Ngày đầu tháng 8 */
                ngayValue = 31;
                thangValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                ngayValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            }
        } else { /* Các tháng còn lại có 31 ngày */
            if (ngayValue == 1) { /* Ngày đầu các tháng có 31 ngày */
                ngayValue = 30;
                thangValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                ngayValue -= 1;
                getMyEle("bt1_showKetQuaNgayHomQua").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            }
        }
    }
    getMyEle("bt1_showKetQuaNgayHomQua").style.display = "block";
    getMyEle("bt1_showKetQuaNgayMai").style.display = "none";
}

function tinhNgayMai() {
    var ngayValue = getMyEle("number_ngay_bt1").value * 1;
    var thangValue = getMyEle("number_thang_bt1").value * 1;
    var namValue = getMyEle("number_nam_bt1").value * 1;

    if (ngayValue <= 0 || ngayValue > 31) {
        getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày không chính xác. Vui lòng nhập lại`
    } else if (thangValue <= 0 || thangValue > 12) {
        getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Tháng không chính xác. Vui lòng nhập lại`
    } else if (namValue <= 0) {
        getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Năm không chính xác. Vui lòng nhập lại`
    } else {
        if (thangValue == 12) { /* Do tháng 12 là tháng cuối năm, nếu rơi vào ngày cuối tháng 12 thì năm của ngày mai sẽ là năm mới nên xử lý riêng */
            if (ngayValue == 31) { /* Ngày cuối tháng 12 */
                ngayValue = 1;
                thangValue = 1;
                namValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                ngayValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            }
        } else if (thangValue == 2) { /* Tháng 2 có 28 ngày */
            if (ngayValue < 28) { /* Ngày cuối tháng 2 */
                ngayValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else if (ngayValue == 28) {
                ngayValue = 1;
                thangValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Tháng 2  chỉ có tối đa 28 ngày. Vui lòng nhập lại `
            }
        } else if (thangValue == 4 || thangValue == 6 || thangValue == 9 || thangValue == 11) { /*Các tháng có 30 ngày */
            if (ngayValue < 30) { /* Ngày cuối các tháng có 30 ngày */
                ngayValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else if (ngayValue == 30) {
                ngayValue = 1;
                thangValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Tháng ${thangValue} chỉ có tối đa 30 ngày. Vui lòng nhập lại`
            }
        } else { /* Các tháng còn lại có 31 ngày */
            if (ngayValue == 31) { /* Ngày cuối các tháng có 31 ngày */
                ngayValue = 1;
                thangValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            } else {
                ngayValue += 1;
                getMyEle("bt1_showKetQuaNgayMai").innerHTML = `Ngày ${ngayValue} tháng ${thangValue} năm ${namValue}`
            }
        }
    }
    getMyEle("bt1_showKetQuaNgayMai").style.display = "block";
    getMyEle("bt1_showKetQuaNgayHomQua").style.display = "none";
}

// Bài tập 2
function tinhNgay() {
    var thangValue = getMyEle("number_thang_bt2").value * 1;
    var namValue = getMyEle("number_nam_bt2").value * 1;
    var ngayValue;

    if (thangValue <= 0 || thangValue > 12) {
        getMyEle("show-ket-qua-bt2").innerHTML = `Tháng ko chính xác. Vui lòng nhập lại`
    } else if (namValue <= 0) {
        getMyEle("show-ket-qua-bt2").innerHTML = `Năm ko chính xác. Vui lòng nhập lại`
    } else {
        if (namValue % 4 == 0 && namValue % 100 != 0) { /* Kiểm tra năm nhuận cách 1 */
            if (thangValue == 2) {
                ngayValue = 29;
            } else if (thangValue == 4 || thangValue == 6 || thangValue == 9 || thangValue == 11) {
                ngayValue = 30;
            } else {
                ngayValue = 31;
            }
        } else if (namValue % 400 == 0) { /* Kiểm tra năm nhuận cách 2 */
            if (thangValue == 2) {
                ngayValue = 29;
            } else if (thangValue == 4 || thangValue == 6 || thangValue == 9 || thangValue == 11) {
                ngayValue = 30;
            } else {
                ngayValue = 31;
            }
        } else { /* Năm thường */
            if (thangValue == 2) {
                ngayValue = 28;
            } else if (thangValue == 4 || thangValue == 6 || thangValue == 9 || thangValue == 11) {
                ngayValue = 30;
            } else {
                ngayValue = 31;
            }
        }
        getMyEle("show-ket-qua-bt2").style.display = "block";
        getMyEle("show-ket-qua-bt2").innerHTML = ` Tháng ${thangValue} năm ${namValue} có ${ngayValue} ngày`
    }
}

// Bài tập 3
function docSoNguyen() {
    //932
    var number = getMyEle("soNguyenDuong").value * 1;
    var soHangTram = Math.floor(number / 100);
    var soHangChuc = Math.floor((number % 100) / 10);
    var soHangDonVi = (number % 100) % 10;

    var cachDocSoHangTram;
    if (soHangTram == 0) {
        cachDocSoHangTram = "";
    } else if (soHangTram == 1) {
        cachDocSoHangTram = "Một trăm";
    } else if (soHangTram == 2) {
        cachDocSoHangTram = "Hai trăm";
    } else if (soHangTram == 3) {
        cachDocSoHangTram = "Ba trăm";
    } else if (soHangTram == 4) {
        cachDocSoHangTram = "Bốn trăm";
    } else if (soHangTram == 5) {
        cachDocSoHangTram = "Năm trăm";
    } else if (soHangTram == 6) {
        cachDocSoHangTram = "Sáu trăm";
    } else if (soHangTram == 7) {
        cachDocSoHangTram = "Bảy trăm";
    } else if (soHangTram == 8) {
        cachDocSoHangTram = "Tám trăm";
    } else if (soHangTram == 9) {
        cachDocSoHangTram = "Chín trăm";
    }

    var cachDocSoHangChuc;
    if (soHangChuc == 0) {
        if (soHangTram == 0 || soHangDonVi == 0) {
            cachDocSoHangChuc = "";
        } else if (soHangTram != 0 && soHangDonVi != 0) {
            cachDocSoHangChuc = "lẻ";
        }
    } else if (soHangChuc == 1) {
        cachDocSoHangChuc = "mười";
    } else if (soHangChuc == 2) {
        cachDocSoHangChuc = "hai mươi";
    } else if (soHangChuc == 3) {
        cachDocSoHangChuc = "ba mươi";
    } else if (soHangChuc == 4) {
        cachDocSoHangChuc = "bốn mươi";
    } else if (soHangChuc == 5) {
        cachDocSoHangChuc = "năm mươi";
    } else if (soHangChuc == 6) {
        cachDocSoHangChuc = "sáu mươi";
    } else if (soHangChuc == 7) {
        cachDocSoHangChuc = "bảy mươi";
    } else if (soHangChuc == 8) {
        cachDocSoHangChuc = "tám mươi";
    } else if (soHangChuc == 9) {
        cachDocSoHangChuc = "chín mươi";
    }

    var cachDocSoHangDonVi;
    if (soHangDonVi == 0) {
        cachDocSoHangDonVi = "";
    } else if (soHangDonVi == 1) {
        cachDocSoHangDonVi = "một";
    } else if (soHangDonVi == 2) {
        cachDocSoHangDonVi = "hai";
    } else if (soHangDonVi == 3) {
        cachDocSoHangDonVi = "ba";
    } else if (soHangDonVi == 4) {
        cachDocSoHangDonVi = "bốn";
    } else if (soHangDonVi == 5) {
        if (soHangChuc == 0) {
            cachDocSoHangDonVi = "năm";
        } else if (soHangChuc != 0) {
            cachDocSoHangDonVi = "lăm";
        }
    } else if (soHangDonVi == 6) {
        cachDocSoHangDonVi = "sáu";
    } else if (soHangDonVi == 7) {
        cachDocSoHangDonVi = "bảy";
    } else if (soHangDonVi == 8) {
        cachDocSoHangDonVi = "tám";
    } else if (soHangDonVi == 9) {
        cachDocSoHangDonVi = "chín";
    }

    getMyEle("show-ket-qua-bt3").style.display = "block";
    getMyEle("show-ket-qua-bt3").innerHTML = `${cachDocSoHangTram} ${cachDocSoHangChuc} ${cachDocSoHangDonVi}`
}

// Bài tập 4
function timKhoangCachXaNhat() {
    var hoTenSv1 = getMyEle("name_sv1").value;
    var toaDoXSv1 = getMyEle("toadoX_sv1").value * 1;
    var toaDoYSv1 = getMyEle("toadoY_sv1").value * 1;

    var hoTenSv2 = getMyEle("name_sv2").value;
    var toaDoXSv2 = getMyEle("toadoX_sv2").value * 1;
    var toaDoYSv2 = getMyEle("toadoY_sv2").value * 1;

    var hoTenSv3 = getMyEle("name_sv3").value;
    var toaDoXSv3 = getMyEle("toadoX_sv3").value * 1;
    var toaDoYSv3 = getMyEle("toadoY_sv3").value * 1;

    var toaDoXTruongHoc = getMyEle("toadoX_truonghoc").value * 1;
    var toaDoYTruongHoc = getMyEle("toadoY_truonghoc").value * 1;

    var khoangCachSv1 = Math.sqrt(Math.pow(toaDoXTruongHoc - toaDoXSv1,2) + Math.pow(toaDoYTruongHoc - toaDoYSv1,2));
    var khoangCachSv2 = Math.sqrt(Math.pow(toaDoXTruongHoc - toaDoXSv2,2) + Math.pow(toaDoYTruongHoc - toaDoYSv2,2));
    var khoangCachSv3 = Math.sqrt(Math.pow(toaDoXTruongHoc - toaDoXSv3,2) + Math.pow(toaDoYTruongHoc - toaDoYSv3,2));
   
    var soSanhKhoangCachSv1VaSv2 = khoangCachSv1 > khoangCachSv2 ? khoangCachSv1 : khoangCachSv2;
    var khoangCachXaNhat = soSanhKhoangCachSv1VaSv2 > khoangCachSv3 ? soSanhKhoangCachSv1VaSv2 : khoangCachSv3;

    if(khoangCachXaNhat == khoangCachSv1) {
        getMyEle("show-ket-qua-bt4").innerHTML = `Sinh viên ${hoTenSv1} xa trường nhất`
    } else if (khoangCachXaNhat == khoangCachSv2) {
        getMyEle("show-ket-qua-bt4").innerHTML = `Sinh viên ${hoTenSv2} xa trường nhất`
    } else {
        getMyEle("show-ket-qua-bt4").innerHTML = `Sinh viên ${hoTenSv3} xa trường nhất`
    }

    getMyEle("show-ket-qua-bt4").style.display = "block";
}
